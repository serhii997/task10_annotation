package com.movchan.model;

public interface ControllerAnnotation {
    void printAnnotation();
    void printAnnotationField();
    void printInvokeMethod();
    void createdObject();
}

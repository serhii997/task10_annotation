package com.movchan.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyGenericClass<T> {
    private final static Logger LOGGER = LogManager.getLogger(MyGenericClass.class);
    private T obj;

    private final Class<T> clazz;

    public MyGenericClass( Class<T> clazz) {
        this.clazz = clazz;
        try {
            //obj=clazz.newInstance(); from 8 Java -  do not used
            obj= clazz.getConstructor().newInstance();
            LOGGER.info("The declared fields of "+ clazz.getSimpleName()+ " are ");
            Field[] fields= clazz.getDeclaredFields();
            for(Field field: fields) {
                LOGGER.info(field.getName());
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public MyGenericClass(T obj, Class<T> clazz) {
        this.obj = obj;
        this.clazz = clazz;
    }
}

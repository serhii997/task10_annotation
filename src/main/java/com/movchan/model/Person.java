package com.movchan.model;

import com.movchan.annotation.MyAnnotation;

public class Person {
    @MyAnnotation(name = "Petro")
    private String name;
    @MyAnnotation(live = "Krakow")
    private String live;
    private int old;
    private boolean married;

    public Person() {
    }


    private void info(){
        System.out.println("Hello");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public int getOld() {
        return old;
    }

    public void setOld(int old) {
        this.old = old;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", live='" + live + '\'' +
                ", old=" + old +
                ", married=" + married +
                '}';
    }

    public int[] getLuckyNumber(String name, int...number){
        System.out.println(name);
        return number;
    }

    public String[] getLuckyNumber(String...name){
        return name;
    }
}

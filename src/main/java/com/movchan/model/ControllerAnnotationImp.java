package com.movchan.model;

import com.movchan.annotation.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ControllerAnnotationImp implements ControllerAnnotation {
    private final static Logger LOGGER = LogManager.getLogger(ControllerAnnotation.class);

    public ControllerAnnotationImp() {
        createdObject();
    }

    public void printAnnotation() {
        Class clazz = Person.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if(field.isAnnotationPresent(MyAnnotation.class)){
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                LOGGER.info("field = "+field.getName());
            }
        }
    }

    @Override
    public
    void printAnnotationField() {
        Class clazz = Person.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if(field.isAnnotationPresent(MyAnnotation.class)){
                MyAnnotation myAnnotation = field.getAnnotation(MyAnnotation.class);
                String name = myAnnotation.name();
                String live = myAnnotation.live();
                int age = myAnnotation.age();
                LOGGER.info("field = " + field.getName());
                LOGGER.info("Name = " + name);
                LOGGER.info("live = " + live);
                LOGGER.info("age = " + age);
            }
        }
    }

    @Override
        public void printInvokeMethod() {
            Class clazz = Person.class;
            try {
                Method method = clazz.getDeclaredMethod("getLive");
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
    }
    @Override
    public void createdObject(){
        LOGGER.info("Created object");
        Person person = new Person();

        printReflUnknownObject(person);
    }


    private void printReflUnknownObject(Object o) {
        LOGGER.info("Created object");
        Person person = new Person();
        try {
            Class clazz = o.getClass();
            LOGGER.info("The name of class is " + clazz.getName());

            Constructor constructor = clazz.getConstructor();
            LOGGER.info("The name of constructor " + constructor.getName());

            LOGGER.info("Methods");
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                LOGGER.info("The name of method " + method.getName());
            }

            LOGGER.info("Fields");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field: fields) {
                LOGGER.info("The name of fields " + field.getName());
            }
                LOGGER.info(o);
            Method callMethod = clazz.getDeclaredMethod("getLive");
            callMethod.invoke(o);
            Method callMethod1 = clazz.getDeclaredMethod("info");
            callMethod1.setAccessible(true);
            callMethod1.invoke(o);

            LOGGER.info("This output field");
            fields[0].setAccessible(true);
            if(fields[0].getType() == String.class){
                fields[0].set(o,"Ivan");
            }
            LOGGER.info(o);

            LOGGER.info("Last task");
            Method method = clazz.getDeclaredMethod("getLuckyNumber", new Class[]{String.class, int[].class});
            int[] arr = {1,2,3};
            method.setAccessible(true);
            int[] newArr = (int[]) method.invoke(o,"Andrey", arr);
            for (int i = 0; i < newArr.length; i++) {
                LOGGER.info(newArr[i]);
            }

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}

package com.movchan;

import com.movchan.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().run();
    }
}

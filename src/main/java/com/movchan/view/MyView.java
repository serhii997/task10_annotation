package com.movchan.view;

import com.movchan.model.ControllerAnnotation;
import com.movchan.model.ControllerAnnotationImp;
import com.movchan.model.MyGenericClass;
import com.movchan.model.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {
    private final static Logger LOGGER = LogManager.getLogger(MyGenericClass.class);

    private static Scanner scanner = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private ControllerAnnotation controllerAnnotation = new ControllerAnnotationImp();
    private ResourceBundle bundle;
    private Locale locale;

    public MyView() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::internationalizeMenuUkraine);
        methodMenu.put("2", this::internationalizeMenuEnglish);
        methodMenu.put("3", (() -> controllerAnnotation.printAnnotation()));
        methodMenu.put("4", (() -> controllerAnnotation.createdObject()));
        methodMenu.put("5", (this::pressButton4));
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
    }

    private void internationalizeMenuUkraine(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void pressButton4() {
        MyGenericClass<Person> obj = new MyGenericClass<>(Person.class);

    }

    private void printMenu() {
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
    }
    public void run(){
        String input ="";
        printMenu();
        do {
            try {
                input = MyView.scanner.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                //LOGGER.error(bundleWords.getString("wrongcommand"));
                printMenu();
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }
}
